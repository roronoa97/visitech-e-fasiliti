@extends('layouts.app', ['title' => 'booking'])

@push('styles')

@endpush

@section('title')
    Tempah Bilik
@endsection

@section('content')
<div class="container-fluid">
    @include('components.flash-message')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Tempah Bilik</h4>
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <ul>
                                @foreach($errors->all() as $error)
                                <li><strong>{{ $error }}</strong></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('booking.store') }}" method="POST">
                        @csrf
                        <div class="form-body mb-4">
                            <div class="row mb-3">
                                <label class="col-lg-2">Bilik*</label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="room_id">
                                        @foreach($rooms as $room)
                                            <option value="{{ $room->id }}" {{ old('room_id') == $room->id ? 'selected' : ''}}>{{ $room->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Tujuan*</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control @error('reasons') is-invalid @enderror" name="reasons" value="{{ old('reasons') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Tarikh Mula*</label>
                                <div class="col-lg-4">
                                    <input type="date" class="form-control @error('start_date') is-invalid @enderror" name="start_date" value="{{ old('start_date') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Tarikh Akhir*</label>
                                <div class="col-lg-4">
                                    <input type="date" class="form-control @error('end_date') is-invalid @enderror" name="end_date" value="{{ old('end_date') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Masa Mula*</label>
                                <div class="col-lg-4">
                                    <input type="time" class="form-control @error('start_time') is-invalid @enderror" name="start_time" value="{{ old('start_time') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Masa Akhir*</label>
                                <div class="col-lg-4">
                                    <input type="time" class="form-control @error('end_time') is-invalid @enderror" name="end_time" value="{{ old('end_time') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Bilangan Pengguna*</label>
                                <div class="col-lg-4">
                                    <input type="number" class="form-control @error('attendees') is-invalid @enderror" name="attendees" value="{{ old('attendees') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="text-left">
                                <button type="submit" class="btn btn-info">Hantar Permohonan</button>
                                <button type="reset" class="btn btn-dark">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
@endpush
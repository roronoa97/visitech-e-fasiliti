@extends('layouts.app', ['title' => 'booking'])

@push('styles')

@endpush

@section('title')
    Permohonan
@endsection

@section('content')
<div class="container-fluid">
    @include('components.flash-message')
    @if(Auth::user()->role == 'staf')
    <a href="{{ route('booking.create') }}" class="btn btn-primary btn-lg mb-3 shadow">Tempah Bilik</a>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Permohonan</h4>
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <ul>
                                @foreach($errors->all() as $error)
                                <li><strong>{{ $error }}</strong></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Bilik</th>
                                    <th scope="col">Tujuan</th>
                                    <th scope="col">Tarikh Mula (masa)</th>
                                    <th scope="col">Tarikh Akhir (masa)</th>
                                    <th scope="col">Jumlah Kehadiran</th>
                                    <th scope="col">Status</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($bookings as $booking)
                                <tr>
                                    @php
                                        $status = $booking->status;
                                        if($status == 0){
                                            $s = 'Diproses';
                                        } else if ($status == 1){
                                            $s = 'Diterima';
                                        } else if ($status == 2){
                                            $s = 'Ditolak';
                                        } else if ($status == 3){
                                            $s = 'Dibatalkan';
                                        }

                                    @endphp
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ App\Models\Room::find($booking->room_id)->name }}</td>
                                    <td>{{ $booking->reasons }}</td>
                                    <td>{{ $booking->start_date }} ( {{ $booking->start_time }} )</td>
                                    <td>{{ $booking->end_date }} ( {{ $booking->end_time }} )</td>
                                    <td>{{ $booking->attendees }}</td>
                                    <td>{{ $s }}</td>
                                    <td>
                                        @if($s == 'Diproses')
                                            @if(Auth::user()->role == 'pengesah')
                                                <a href="{{ route('booking.approved', ['user_id' => $booking->user_id, 'room_id' => $booking->room_id ])}}" class="btn btn-sm btn-success">Sahkan</a>
                                                <a href="{{ route('booking.rejected', ['user_id' => $booking->user_id, 'room_id' => $booking->room_id ])}}" class="btn btn-sm btn-danger">Tolak</a>
                                            @endif
                                        @elseif($s == 'Diterima')
                                            <a href="{{ route('booking.canceled', ['user_id' => $booking->user_id, 'room_id' => $booking->room_id ])}}" class="btn btn-sm btn-info">Batalkan</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('title')
    Tambah Pengguna
@endsection

@section('content')
<div class="container-fluid">
    @include('components.flash-message')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Tambah Pengguna</h4>
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <ul>
                                @foreach($errors->all() as $error)
                                <li><strong>{{ $error }}</strong></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('admin.users.store') }}" method="POST">
                        @csrf
                        <div class="form-body mb-4">
                            <div class="row mb-3">
                                <label class="col-lg-2">ID Staf*</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control @error('staff_id') is-invalid @enderror" name="staff_id" value="{{ old('staff_id') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Emel*</label>
                                <div class="col-lg-4">
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Peranan*</label>
                                <div class="col-lg-4">
                                    <select class="form-control" name="role">
                                        <option value="staf" {{ old('role') == 'staf' ? 'selected' : ''}}>Staf</option>
                                        <option value="pengesah" {{ old('role') == 'pengesah' ? 'selected' : ''}}>Pengesah</option>
                                        <option value="admin" {{ old('role') == 'admin' ? 'selected' : ''}}>Admin</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Nama*</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Jawatan*</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control @error('position') is-invalid @enderror" name="position" value="{{ old('position') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Bahagian*</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control @error('department') is-invalid @enderror" name="department" value="{{ old('department') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Tarikh Lahir*</label>
                                <div class="col-lg-4">
                                    <input type="date" class="form-control @error('dob') is-invalid @enderror" name="dob" value="{{ old('dob') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Jantina*</label>
                                <div class="col-lg-4">
                                    <select class="form-control" name="gender">
                                        <option value="Lelaki" {{ old('gender') == 'Lelaki' ? 'selected' : ''}}>Lelaki</option>
                                        <option value="Perempuan" {{ old('gender') == 'Perempuan' ? 'selected' : ''}}>Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Tel Pejabat</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control @error('office_no') is-invalid @enderror" name="office_no" value="{{ old('office_no') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Tel Bimbit</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control @error('mobile_no') is-invalid @enderror" name="mobile_no" value="{{ old('mobile_no') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="text-left">
                                <button type="submit" class="btn btn-info">Hantar</button>
                                <button type="reset" class="btn btn-dark">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
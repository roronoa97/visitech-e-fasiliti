@extends('layouts.app')

@section('title')
    Tambah Bilik
@endsection

@section('content')
<div class="container-fluid">
    @include('components.flash-message')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Tambah Bilik</h4>
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <ul>
                                @foreach($errors->all() as $error)
                                <li><strong>{{ $error }}</strong></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('admin.rooms.store') }}" method="POST">
                        @csrf
                        <div class="form-body mb-4">
                            <div class="row mb-3">
                                <label class="col-lg-2">Nama Bilik*</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Jenis Bilik*</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control @error('type') is-invalid @enderror" name="type" value="{{ old('type') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Lokasi*</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control @error('location') is-invalid @enderror" name="location" value="{{ old('location') }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-lg-2">Kapasiti*</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control @error('capacity') is-invalid @enderror" name="capacity" value="{{ old('capacity') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="text-left">
                                <button type="submit" class="btn btn-info">Hantar</button>
                                <button type="reset" class="btn btn-dark">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
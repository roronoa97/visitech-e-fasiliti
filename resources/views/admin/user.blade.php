@extends('layouts.app', ['title' => 'booking'])

@push('styles')

@endpush

@section('title')
    Pengguna
@endsection

@section('content')
<div class="container-fluid">
    @include('components.flash-message')
    <a href="{{ route('admin.users.create') }}" class="btn btn-primary btn-lg mb-3 shadow">Tambah Pengguna</a>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Pengguna</h4>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">ID Staf</th>
                                    <th scope="col">Emel</th>
                                    <th scope="col">Peranan</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $user->staff_id }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->role }}</td>
                                    <td>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
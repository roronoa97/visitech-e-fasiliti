@extends('layouts.app', ['title' => 'booking'])

@push('styles')

@endpush

@section('title')
    Bilik
@endsection

@section('content')
<div class="container-fluid">
    @include('components.flash-message')
    <a href="{{ route('admin.rooms.create') }}" class="btn btn-primary btn-lg mb-3 shadow">Tambah Bilik</a>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Bilik</h4>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Bilik</th>
                                    <th scope="col">Jenis</th>
                                    <th scope="col">Lokasi</th>
                                    <th scope="col">Kapasiti</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($rooms as $room)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $room->name }}</td>
                                    <td>{{ $room->type }}</td>
                                    <td>{{ $room->location }}</td>
                                    <td>{{ $room->capacity }}</td>
                                    <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
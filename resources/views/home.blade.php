@extends('layouts.app', ['title' => 'dashboard'])

@push('styles')
<link href="https://cdn.jsdelivr.net/npm/fullcalendar@5.10.1/main.min.css" rel="stylesheet" />

@endpush
@section('title')
    Dashboard
@endsection

@section('content')
<div class="container-fluid">
    @if(Auth::user()->role == 'staf')
    <a href="{{ route('booking.create') }}" class="btn btn-primary btn-lg mb-3 shadow">Tempah Bilik</a>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body b-l calender-sidebar">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('template/assets/libs/moment/min/moment.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.10.1/main.min.js"></script>
<script>
    $(document).ready(function(){
        var defaultEvents = {!! json_encode($data, true) !!}

        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            initialView: 'dayGridMonth',
            events: defaultEvents,
        });
        calendar.render();
    })
    

    

  </script>
@endpush
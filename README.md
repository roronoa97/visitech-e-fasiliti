**GUIDELINE TO START**

1. Clone this repositories
2. Run composer install & npm install
3. Edit .env
    1. change DB creadentials
    2. add MAILTRAP credential / any other Mail configuration
4. Run cmd - php artisan config:cache
5. Run cmd - php artisan migrate
6. Run cmd - php artisan db:seed --class=UserSeeder
7. Run cmd - php artisan db:seed --class=RoomSeeder
8. Serve application by cmd - php artisan serve

<?php

namespace Database\Seeders;

use App\Models\Profile;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        Profile::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        
        $users = [
            [
                'role' => 'admin',
                'staff_id' => '001',
                'name' => 'Nur Syasya',
                'position' => 'Executive Officer',
                'department' => 'Human Resources',
                'dob' => Carbon::now()->subYears('32')->format('Y-m-d'),
                'gender' => 'Perempuan',
                'office_no' => null,
                'mobile_no' => null,
            ],
            [
                'role' => 'pengesah',
                'staff_id' => '002',
                'name' => 'Siti Aishah',
                'position' => 'Executive Officer',
                'department' => 'Human Resources',
                'dob' => Carbon::now()->subYears('43')->format('Y-m-d'),
                'gender' => 'Perempuan',
                'office_no' => null,
                'mobile_no' => null,
            ],
            [
                'role' => 'staf',
                'staff_id' => '003',
                'name' => 'Aliff Ahmad',
                'position' => 'IT Officer',
                'department' => 'Software Department',
                'dob' => Carbon::now()->subYears('27')->format('Y-m-d'),
                'gender' => 'Lelaki',
                'office_no' => null,
                'mobile_no' => null,
            ],
        ];

        foreach($users as $user){
            $u = User::create([
                'staff_id' => $user['staff_id'],
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'email' => $user['role'].'@gmail.com',
                'role' => $user['role'],
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);

            $u->profile()->create([
                'name' => $user['name'],
                'position' => $user['position'],
                'department' => $user['department'],
                'dob' => $user['dob'],
                'gender' => $user['gender'],
                'office_no' => $user['office_no'],
                'mobile_no' => $user['mobile_no'],
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }
    }
}

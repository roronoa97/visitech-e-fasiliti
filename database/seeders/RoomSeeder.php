<?php

namespace Database\Seeders;

use App\Models\Room;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Room::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Room::insert([
            [
                'name' => 'BILIK MESYUARAT CEMPAKA',
                'type' => 'Bilik Mesyuarat',
                'location' => 'Aras 1, Sayap Kanan, Blok A',
                'capacity' => 50,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'BILIK MESYUARAT KASUARINA',
                'type' => 'Bilik Mesyuarat',
                'location' => 'Aras 1, Sayap Kanan, Blok A',
                'capacity' => 50,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'BILIK PERBINCANGAN MELOR',
                'type' => 'Bilik Perbincangan',
                'location' => 'Aras 1, Sayap Kanan, Blok A',
                'capacity' => 50,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'BILIK PERBINCANGAN MAWAR',
                'type' => 'Bilik Perbincangan',
                'location' => 'Aras 1, Sayap Kanan, Blok A',
                'capacity' => 50,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'BILIK PERBINCANGAN MAWAR',
                'type' => 'Makmal Komputer',
                'location' => 'Aras 1, Sayap Kanan, Blok A',
                'capacity' => 50,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ]);
    }
}

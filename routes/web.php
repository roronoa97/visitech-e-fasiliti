<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes(['register' => false]);
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/bookings', [BookingController::class, 'index'])->name('booking.index');
Route::get('/bookings/create', [BookingController::class, 'create'])->name('booking.create');
Route::post('/bookings/store', [BookingController::class, 'store'])->name('booking.store');
Route::get('/bookings/approved/user/{user_id}/room/{room_id}', [BookingController::class, 'approved'])->name('booking.approved');
Route::get('/bookings/rejected/user/{user_id}/room/{room_id}', [BookingController::class, 'rejected'])->name('booking.rejected');
Route::get('/bookings/canceled/user/{user_id}/room/{room_id}', [BookingController::class, 'canceled'])->name('booking.canceled');

Route::get('/users', [AdminController::class, 'users'])->name('admin.users');
Route::get('/rooms', [AdminController::class, 'rooms'])->name('admin.rooms');

Route::get('/users/create', [AdminController::class, 'user_create'])->name('admin.users.create');
Route::post('/users/store', [AdminController::class, 'user_store'])->name('admin.users.store');
Route::get('/users/{id}/edit', [AdminController::class, 'user_edit'])->name('admin.users.edit');
Route::post('/users/{id}/update', [AdminController::class, 'user_update'])->name('admin.users.update');

Route::get('/rooms/create', [AdminController::class, 'room_create'])->name('admin.rooms.create');
Route::post('/rooms/store', [AdminController::class, 'room_store'])->name('admin.rooms.store');
Route::get('/rooms/{id}/edit', [AdminController::class, 'room_edit'])->name('admin.rooms.edit');
Route::post('/rooms/{id}/update', [AdminController::class, 'room_update'])->name('admin.rooms.update');


<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'position',
        'department',
        'dob',
        'gender',
        'office_no',
        'mobile_no'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'type',
        'location',
        'capacity'
    ];

    public function users(){
        return $this->belongsToMany(User::class, 'bookings')->withPivot(['reasons', 'start_date', 'end_date','start_time', 'end_time', 'attendees', 'status']);
    }
}

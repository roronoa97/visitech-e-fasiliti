<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function users(){

        $users = User::all();

        return view('admin.user', compact('users'));
    }

    public function rooms(){

        $rooms = Room::all();

        return view('admin.room', compact('rooms'));
    }

    public function user_create(){

        return view('admin.user-create');
    }

    public function room_create(){

        return view('admin.room-create');
    }

    public function user_store(Request $request){

        $request->validate([
            'staff_id' => 'required',
            'email' => 'required|email',
            'role' => 'required',
            'name' => 'required',
            'position' => 'required',
            'department' => 'required',
            'dob' => 'required|date',
            'gender' => 'required',
            'office_no' => 'nullable',
            'mobile_no' => 'nullable',
        ]);

        $user = User::create([
            'staff_id' => $request->staff_id,
            'email' => $request->email,
            'password' => 'password',
            'role' => $request->role,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $user->profile()->create([
            'name' => $request->name,
            'position' => $request->position,
            'department' => $request->department,
            'dob' => $request->dob,
            'gender' => $request->gender,
            'office_no' => $request->office_no,
            'mobile_no' => $request->mobile_no,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return redirect()->route('admin.users')->with('success', 'Pengguna Berjaya Ditambah');
    }

    public function room_store(Request $request){

        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'location' => 'required',
            'capacity' => 'required'
        ]);

        Room::create([
            'name' => $request->name,
            'type' => $request->type,
            'location' => $request->location,
            'capacity' => $request->capacity,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return redirect()->route('admin.rooms')->with('success', 'Bilik Berjaya Ditambah');

    }


}

<?php

namespace App\Http\Controllers;

use App\Mail\EmelNotification;
use App\Models\Room;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class BookingController extends Controller
{
    public function index(){
        if(Auth::user()->role == 'staf'){
            $bookings = DB::table('bookings')->where('user_id', Auth::id())->get();
        } else if(Auth::user()->role == 'pengesah'){
            $bookings = DB::table('bookings')->get();
        }

        return view('bookings.index', compact('bookings'));
    }
    public function create(){
        $rooms = Room::orderBy('id')->get();

        return view('bookings.create', compact('rooms'));
    }

    public function store(Request $request){
        $data = $request->validate([
            'room_id' => 'required',
            'reasons' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'start_time' => 'required|date_format:H:i',
            'end_time' => 'required|date_format:H:i',
            'attendees' => 'required|integer'
        ]);

        try{
            $room = Room::find($data['room_id']);

            if(!empty($room)){
                $user = Auth::user();

                $is_exists = DB::table('bookings')
                ->where('room_id', '=',  $data['room_id'])
                ->where(function($query) use ($data){
                    $query->where('start_date', '>=', $data['start_date'])->where('end_date', '<=', $data['start_date'])
                    ->orWhere('start_date', '<=', $data['end_date'])->where('end_date', '>=', $data['end_date']);
                })
                ->where('status', '!=', 2)
                ->where('status', '!=', 3)
                ->first();

                if(empty($is_exists)){
                    $user->rooms()->attach($data['room_id'], [
                        'reasons' => $data['reasons'],
                        'start_date' => $data['start_date'],
                        'end_date' => $data['end_date'],
                        'start_time' => $data['start_time'],
                        'end_time' => $data['end_time'],
                        'attendees' => $data['attendees'],
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);
                } else {
                    return back()->with('warning','Permohonan Tidak Berjaya. Tiada kekosongan buat masa ini');
                }

                Log::info($is_exists);
                return back()->with('success','Permohonan Telah Dihantar');
            }else{
                return back()->with('warning','Sila masukkan maklumat bilik yang sah');
            }
        }catch(\Exception $error){
            Log::error($error);
            return back()->with('error','Ralat. Sila hubungi admin sistem');

        }
    }

    public function approved($user_id, $room_id){

        $user = User::find($user_id);
        $room = Room::find($room_id);

        $user->rooms()->updateExistingPivot($room_id, [
            'status' => 1
        ]);

        $details = [
            'room' => $room->name,
            'status' => 'Diterima'
        ];

        Mail::to($user->email)->send(new EmelNotification($details));

        return redirect()->back()->with('success', 'Permohonan Berjaya Dikemaskini');
    }

    public function rejected($user_id, $room_id){

        $user = User::find($user_id);
        $room = Room::find($room_id);

        $user->rooms()->updateExistingPivot($room_id, [
            'status' => 2
        ]);

        $details = [
            'room' => $room->name,
            'status' => 'Ditolak'
        ];

        Mail::to($user->email)->send(new EmelNotification($details));

        return redirect()->back()->with('success', 'Permohonan Berjaya Dikemaskini');
    }

    public function canceled($user_id, $room_id){

        $user = User::find($user_id);
        $room = Room::find($room_id);

        $user->rooms()->updateExistingPivot($room_id, [
            'status' => 3
        ]);

        $details = [
            'room' => $room->name,
            'status' => 'Dibatalkan'
        ];

        Mail::to($user->email)->send(new EmelNotification($details));

        return redirect()->back()->with('success', 'Permohonan Berjaya Dikemaskini');
    }
}

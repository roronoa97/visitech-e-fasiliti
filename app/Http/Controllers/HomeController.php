<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $bookings = DB::table('bookings')->where('status', 1)->get();

        $data = [];
        foreach($bookings as $booking){
            $is_user = null;

            if(Auth::id() == $booking->user_id){
                $is_user = '(you)';
            }
            $data[] = [
                'title' => Room::find($booking->room_id)->name . $is_user,
                'start' => $booking->start_date,
                'end' => $booking->end_date . ' 23:59:00'
            ];
        }   

        return view('home', compact('data'));
    }
}
